FROM alpine:3.18 

# prepare unrar
SHELL ["/bin/ash", "-o", "pipefail", "-c"]
WORKDIR /build
RUN \
  apk add --no-cache curl build-base && \
  curl -L -o /build/unrar.tar.gz -L "https://www.rarlab.com/rar/unrarsrc-6.2.12.tar.gz" && \  
  tar xf /build/unrar.tar.gz -C /build --strip-components=1 && \
  sed -i 's|LDFLAGS=-pthread|LDFLAGS=-pthread -static|' makefile && \
  make && \
  install -v -m755 unrar /usr/bin && \
  rm -rf /build/* && \
  apk del --purge curl build-base

